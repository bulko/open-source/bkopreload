$.bkoPreload = function( imgs )
{
	var $cacheArea;
	var length = imgs.length;
	var cacheCursor = length / 2 ;

	function isCached( src )
	{
		var image = new Image();
		image.src = src;
		return image.complete;
	}

	function initCacheArea()
	{
		$("body").append("<div class='bkoCacheArea' style='"
			+ "display:none;"
			+ "visibility:hidden;"
			+ "overflow:hidden;"
			+ "height:0px;"
			+ "width:0px;"
			+"'></div>"
		);
		$cacheArea = $("body").find(".bkoCacheArea");
	}

	function loadItem( src )
	{
		$cacheArea.append("<img class='bkoPreload' src='" + src + "' alt='bkoPreload'/>");
	}

	function recursiveLoading()
	{
		if( cacheCursor < length )
		{
			console.log( cacheCursor, imgs[ cacheCursor ] );
			loadItem( imgs[ cacheCursor ] );
			cacheCursor++;
			$cacheArea.find(".bkoPreload").on('load error', function(){ recursiveLoading(); } );
		}
	}

	function initCacheCursor()
	{
		var savePoss = length;
		while( cacheCursor > 1 && cacheCursor < length && Math.abs( savePoss - cacheCursor ) >= 1 )
		{
			savePoss = cacheCursor;
			if ( isCached( imgs[ cacheCursor ] ) )
			{
				cacheCursor = Math.round( cacheCursor + ( cacheCursor / 2 ) );
			}
			else
			{
				cacheCursor = Math.round( cacheCursor - ( cacheCursor / 2 ) );
			}
			console.log( isCached( imgs[ cacheCursor ] ) );
			console.log( "dif", Math.abs(savePoss - cacheCursor) );
			console.log( "savePoss", savePoss );
			console.log( "cacheCursor", cacheCursor );
		}
	}

	function init()
	{
		initCacheCursor();
		if( cacheCursor < length )
		{
			initCacheArea();
			recursiveLoading();
		}
	}

	init();
}

